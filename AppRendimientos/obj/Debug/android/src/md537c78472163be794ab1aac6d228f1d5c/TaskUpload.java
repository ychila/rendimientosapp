package md537c78472163be794ab1aac6d228f1d5c;


public class TaskUpload
	extends android.os.AsyncTask
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onPreExecute:()V:GetOnPreExecuteHandler\n" +
			"n_doInBackground:([Ljava/lang/Object;)Ljava/lang/Object;:GetDoInBackground_arrayLjava_lang_Object_Handler\n" +
			"n_onPostExecute:(Ljava/lang/Object;)V:GetOnPostExecute_Ljava_lang_Object_Handler\n" +
			"";
		mono.android.Runtime.register ("AppRendimientos.Task.TaskUpload, AppRendimientos, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", TaskUpload.class, __md_methods);
	}


	public TaskUpload ()
	{
		super ();
		if (getClass () == TaskUpload.class)
			mono.android.TypeManager.Activate ("AppRendimientos.Task.TaskUpload, AppRendimientos, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public TaskUpload (android.app.ProgressDialog p0, android.database.Cursor p1)
	{
		super ();
		if (getClass () == TaskUpload.class)
			mono.android.TypeManager.Activate ("AppRendimientos.Task.TaskUpload, AppRendimientos, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.App.ProgressDialog, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Database.ICursor, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0, p1 });
	}


	public void onPreExecute ()
	{
		n_onPreExecute ();
	}

	private native void n_onPreExecute ();


	public java.lang.Object doInBackground (java.lang.Object[] p0)
	{
		return n_doInBackground (p0);
	}

	private native java.lang.Object n_doInBackground (java.lang.Object[] p0);


	public void onPostExecute (java.lang.Object p0)
	{
		n_onPostExecute (p0);
	}

	private native void n_onPostExecute (java.lang.Object p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
