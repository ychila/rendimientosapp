﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using AppRendimientos.Adapters;
using AppRendimientos.Class;
using ZXing.Mobile;

namespace AppRendimientos.Fragments
{
    public class Calidad : Fragment
    {
        View view;
        GridView gCalidad;
        List<ListData> lstData = new List<ListData>();
        Fragment mCurrentFragment;
        TextView txtTitulo;
        ConexionLocal db;
        EditText etColaborator;
        string textColaborador;
        Colaborador FgColaborador;
        int CantidadID;
        int businessUnitID = 0;
        int PlantProductID = 0;
        int BouquetTypeID = 0;
        public Calidad(EditText etColaborator, string textColaborador, int CantidadID)
        {
            this.etColaborator = etColaborator;
            this.textColaborador = textColaborador;
            this.CantidadID = CantidadID;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.FgData, container, false);
            txtTitulo = (TextView)view.FindViewById(Resource.Id.txtTitulo);
            gCalidad = (GridView)view.FindViewById(Resource.Id.gData);
            gCalidad.NumColumns = 2;
            txtTitulo.Text = "Selección de Calidad";

            gCalidad.ItemClick += GCantidad_ItemClick;

            db = new ConexionLocal(Application.Context);
            ICursor cursorData = db.getParameterUnit();
            if (cursorData.MoveToFirst())
            {
                do
                {
                    businessUnitID = cursorData.GetInt(0);
                    PlantProductID = cursorData.GetInt(1);
                    BouquetTypeID = cursorData.GetInt(2);
                } while (cursorData.MoveToNext());
            }

            cargarCalidad();
            return view;
        }

        private void GCantidad_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var l = lstData[e.Position];
            VariablesGlobales.vgQtyStemID = l.id;
            DateTime fechaM = DateTime.Now;
            String DateRegister = string.Format("{0:yyyy/MM/dd HH:mm:ss}", fechaM);

            db = new ConexionLocal(Application.Context);


            db.SavePerformance(businessUnitID, PlantProductID, textColaborador, CantidadID, l.id, DateRegister, BouquetTypeID, 0);
            FgColaborador = new Colaborador();
            ReplaceFragment(FgColaborador);

        }

        private void cargarCalidad()
        {
            db = new ConexionLocal(Application.Context);
            lstData.Clear();

            ICursor cursorData = db.queryQuality(PlantProductID);
            if (cursorData.MoveToFirst())
            {
                do
                {
                    lstData.Add(new ListData(cursorData.GetInt(0), cursorData.GetString(1), 0));
                } while (cursorData.MoveToNext());
            }
            gCalidad.Adapter = new AdData(Activity, lstData);
            //db = new ConexionLocal(Application.Context);

            //lstData.Clear();

            //lstData.Add(new ListData(35, "35", 0));
            //lstData.Add(new ListData(40, "40", 0));
            //lstData.Add(new ListData(45, "45", 0));
            //lstData.Add(new ListData(50, "50", 0));
            //lstData.Add(new ListData(55, "55", 0));
            //lstData.Add(new ListData(60, "60", 0));
            //lstData.Add(new ListData(65, "65", 0));
            //lstData.Add(new ListData(70, "70", 0));
            //gCalidad.Adapter = new AdData(Activity, lstData);
        }

        public void ReplaceFragment(Fragment fragment)
        {
            if (fragment.IsVisible)
            {
                return;
            }
            var trans = FragmentManager.BeginTransaction();
            trans.Replace(Resource.Id.fragmentContainer, fragment);
            trans.AddToBackStack(null);
            trans.Commit();

            mCurrentFragment = fragment;
        }

    }
}