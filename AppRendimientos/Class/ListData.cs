﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AppRendimientos.Class
{
    public class ListData
    {
        int _id;
        string _nombre;
        int _estado;
        /*CONSTRUCTOR*/
        public ListData(int id,
                         string nombre,
                         int estado
                         )
        {

            this._id = id;
            this._nombre = nombre;
            this._estado = estado;
        }

        /*  PROPIEDADES*/
        public int id
        {
            get
            {
                return _id;
            }
            set
            {

                _id = value;
            }
        }

        public string nombre
        {
            get
            {
                return _nombre;
            }
            set
            {
                _nombre = value;
            }
        }

        public int estado
        {
            get
            {
                return _estado;
            }
            set
            {
                _estado = value;
            }
        }
    }
}