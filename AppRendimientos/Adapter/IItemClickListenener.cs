﻿using Android.Views;

namespace AppRendimientos.Adapter
{
    public interface IItemClickListenener
    {
        void OnClick(View itemView, int position, bool isLongClick);
    }
}