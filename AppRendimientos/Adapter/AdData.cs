﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AppRendimientos.Class;

namespace AppRendimientos.Adapters
{
    internal class AdData : BaseAdapter<ListData>
    {
        private Activity context;
        private List<ListData> lstPlagas;
        private ListData item;
        public AdData(Activity context, List<ListData> lstPlagas)
        {
            this.context = context;
            this.lstPlagas = lstPlagas;
        }

        public override ListData this[int position]
        {
            get { return lstPlagas[position]; }
        }

        public override int Count
        {
            get { return lstPlagas.Count; }
        }


        public override long GetItemId(int position)
        {
            return position;
        }
        
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            item = lstPlagas[position];
            View view = convertView;
            if (view == null)
                view = context.LayoutInflater.Inflate(Resource.Layout.ListData, null);

            //view.FindViewById<TextView>(Resource.Id.IDPlaga).Text = Convert.ToString(item.id_plaga);
            view.FindViewById<TextView>(Resource.Id.txtNombre).Text = Convert.ToString(item.nombre);
           

            return view;
        }
    }
}