﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AppRendimientos.Clases;
using AppRendimientos.Class;
using static Android.Provider.Settings;

namespace AppRendimientos.Task
{
    internal class TaskUpload : AsyncTask
    {
        ProgressDialog progressSync;
        ICursor cursor;
        ICursor cursorOperation;
        SqlConnection conexion;
        ConexionLocal db;
        GeneraTXT GeneraTXT;

        public TaskUpload(ProgressDialog progressSync, ICursor cursorPerformance, ICursor cursorOperation, SqlConnection conexion)
        {
            this.progressSync = progressSync;
            this.cursor = cursorPerformance;
            this.cursorOperation = cursorOperation;
            this.conexion = conexion;
        }

        protected override void OnPreExecute()
        {
            progressSync.Show();
        }

        protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
        {
            SyncData();
            syncDataOperation();
            return "d";
        }

        private void syncDataOperation()
        {
            string sql = "INSERT INTO OperationStartDatetime(BusinessUnitID, BouquetTypeID, OperationStart)VALUES(@BusinessUnitID, @BouquetTypeID, @OperationStart)";

            if (cursorOperation.MoveToFirst())
            {
                do
                {
                    db = new ConexionLocal(Application.Context);
                   
                    using (SqlCommand comando = new SqlCommand(sql, conexion))
                    {
                        DateTime dt = Convert.ToDateTime(cursorOperation.GetString(3));

                        comando.Parameters.Add("@BusinessUnitID", SqlDbType.Int).Value = cursorOperation.GetInt(1);
                        comando.Parameters.Add("@BouquetTypeID", SqlDbType.Int).Value = cursorOperation.GetInt(2);
                        comando.Parameters.Add("@OperationStart", SqlDbType.DateTime).Value = dt;
                       
                        comando.CommandType = CommandType.Text;
                        comando.ExecuteNonQuery();

                        db.UpdateSyncOperations(cursorOperation.GetInt(0));
                    }

                } while (cursorOperation.MoveToNext());
            }
        }

        private void SyncData()
        {
            String id = Secure.GetString(Application.Context.ContentResolver, Secure.AndroidId);
            int tam_var = id.Length;
            String idCelular = id.Substring(tam_var - 2, 2);

            string NombreArchivo = "Ren_" + idCelular + "_" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");
            string sql = "INSERT INTO Performance(BusinnesUnitID, PlantProductID, ProductID, ColourID, QtyStemID,QtyBqtID, QualityID, Collaborator, Date, BouquetTypeID)VALUES(@BusinnesUnitID, @PlantProductID, @ProductID, @ColourID, @QtyStemID,@QtyBqtID, @QualityID, @Collaborator,@Date, @BouquetTypeID)";

            if (cursor.MoveToFirst())
            {
                do
                {
                    db = new ConexionLocal(Application.Context);
                    string Rendimiento = cursor.GetInt(1)+";"+ cursor.GetInt(2) +";"+ cursor.GetString(3) +";"+ cursor.GetString(4) + ";" + cursor.GetInt(5) + ";" + cursor.GetString(6) + ";" + cursor.GetString(7);
                    GeneraTXT = new GeneraTXT();
                    GeneraTXT.GenerarArchivoTxt(Rendimiento, NombreArchivo);


                    using (SqlCommand comando = new SqlCommand(sql, conexion))
                    {
                        DateTime dt = Convert.ToDateTime(cursor.GetString(6));

                        comando.Parameters.Add("@BusinnesUnitID", SqlDbType.Int).Value = cursor.GetInt(1);
                        comando.Parameters.Add("@PlantProductID", SqlDbType.Int).Value = cursor.GetInt(2);
                        comando.Parameters.Add("@ProductID", SqlDbType.Int).Value = 0;
                        comando.Parameters.Add("@ColourID", SqlDbType.Int).Value = 0;
                        comando.Parameters.Add("@QtyStemID", SqlDbType.Int).Value = cursor.GetInt(4);
                        comando.Parameters.Add("@QtyBqtID", SqlDbType.Int).Value = 1;
                        comando.Parameters.Add("@QualityID", SqlDbType.Int).Value = cursor.GetInt(5);
                        comando.Parameters.Add("@Collaborator", SqlDbType.VarChar).Value = cursor.GetString(3);
                        comando.Parameters.Add("@Date", SqlDbType.DateTime).Value = dt;
                        comando.Parameters.Add("@BouquetTypeID", SqlDbType.Int).Value = cursor.GetInt(7);
                        comando.CommandType = CommandType.Text;
                        comando.ExecuteNonQuery();

                        db.UpdateSync(cursor.GetInt(0));
                    }

                } while (cursor.MoveToNext());
            }                     
        }
        protected override void OnPostExecute(Java.Lang.Object result)
        {
            progressSync.Dismiss();
        }

    }
}