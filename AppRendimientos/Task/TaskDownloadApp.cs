﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AppRendimientos.Class;
using Java.Lang;

namespace AppRendimientos.Task
{
    class TaskDownloadApp: AsyncTask
    {
        ProgressDialog progress;
        List<ListDownload> lstDownload;
        ConexionLocal db;

        public TaskDownloadApp(ProgressDialog progress, List<ListDownload> lstDownload)
        {
            this.progress = progress;
            this.lstDownload = lstDownload;
        }

        protected override void OnPreExecute()
        {
            progress.Show();
        }
        protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
        {
            db = new ConexionLocal(Application.Context);
            foreach (ListDownload a in lstDownload)
            {
                switch (a.Nombre)
                {
                    case "PlantProduct":
                            insertPlantProduct();
                        break;                    
                    case "Quality":
                        insertQuality();
                        break;
                    case "QtyStem":
                        insertQtyStem();
                        break;                   
                    case "BusinessUnit":
                        insertBusinessUnit();
                        break;
                    case "BuoquetType":
                        insertBuoquetType();
                        break;
                }
                
            }
            return "y";
        }

        private void insertBuoquetType()
        {
            SqlDataReader AppData;
            ConexionServidor con = new ConexionServidor();

            db.DeleteBuoquetType();

            AppData = con.getBuoquetType();

            if (AppData.HasRows)
            {
                while (AppData.Read())
                {
                    db.SaveBouquetType(AppData.GetInt32(0), AppData.GetString(1));
                }
            }
        }
        private void insertPlantProduct()
        {
            SqlDataReader AppData;
            ConexionServidor con = new ConexionServidor();

            db.DeletePlantProduct();

            AppData = con.getPlantProduct();

            if (AppData.HasRows)
            {
                while (AppData.Read())
                {
                    db.SavePlantProduct(AppData.GetInt32(0), AppData.GetString(1));
                }
            }
        }

       
        private void insertQuality()
        {
            SqlDataReader AppData;
            ConexionServidor con = new ConexionServidor();

            db.DeleteQuality();

            AppData = con.getQuality();

            if (AppData.HasRows)
            {
                while (AppData.Read())
                {
                    db.SaveQuality(AppData.GetInt32(0), AppData.GetString(1), AppData.GetInt32(2));
                }
            }

        }

        private void insertQtyStem()
        {
            SqlDataReader AppData;
            ConexionServidor con = new ConexionServidor();

            db.DeleteQtyStem();

            AppData = con.getQtyStem();

            if (AppData.HasRows)
            {
                while (AppData.Read())
                {
                    db.SaveQtyStem(AppData.GetInt32(0), AppData.GetString(1), AppData.GetInt32(2), AppData.GetInt32(3));
                }
            }
        }

        

        private void insertBusinessUnit()
        {
            SqlDataReader AppData;
            ConexionServidor con = new ConexionServidor();

            db.DeleteBusinessUnit();

            AppData = con.getBusinessUnit();

            if (AppData.HasRows)
            {
                while (AppData.Read())
                {
                    db.SaveBusinessUnit(AppData.GetInt32(0), AppData.GetString(1));
                }
            }
        }

        protected override void OnPostExecute(Java.Lang.Object result)
        {
            progress.Dismiss();
            Toast.MakeText(Application.Context, "Sincronizacion de información ok", ToastLength.Short).Show();
        }
    }
}